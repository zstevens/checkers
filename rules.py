black_pieces = ['○', '◔']
white_pieces = ['●', '◕']
black_king = '◔'
white_king = '◕'

new_game = {
    'board': [
        ['', '●', '', '●', '', '●', '', '●'],
        ['●', '', '●', '', '●', '', '●', ''],
        ['', '●', '', '●', '', '●', '', '●'],
        ['', '', '', '', '', '', '', ''],
        ['', '', '', '', '', '', '', ''],
        ['○', '', '○', '', '○', '', '○', ''],
        ['', '○', '', '○', '', '○', '', '○'],
        ['○', '', '○', '', '○', '', '○', '']
    ],
    'player': 0
}


def get_legal_moves(state):
    # First check for legal jumps
    jumps = legal_jumps(state)

    # If there are jumps available they must be taken
    if jumps:
        return jumps

    board = state['board']
    player = state['player']

    legal = []

    # Iterate over each row on the board
    for row_num, row in enumerate(board):
        # Iterate over each space in the row
        for col_num, col in enumerate(row):
            current_space = (row_num, col_num)
            up_left = (row_num-1, col_num-1)
            up_right = (row_num-1, col_num+1)
            down_left = (row_num+1, col_num-1)
            down_right = (row_num+1, col_num+1)

            # Space occupied by black
            if col in black_pieces and player == 0:
                # Check up/left & up/right diagonals for empty spaces
                if in_bounds(up_left) and board[row_num-1][col_num-1] == '':
                    legal.append((current_space, up_left))
                if in_bounds(up_right) and board[row_num-1][col_num+1] == '':
                    legal.append((current_space, up_right))

            # Check extra moves for black kings
            if col == black_king and player == 0:
                # Check down/left & down/right diagonals for empty spaces
                if in_bounds(down_left) and board[row_num+1][col_num-1] == '':
                    legal.append((current_space, down_left))
                if in_bounds(down_right) and board[row_num+1][col_num+1] == '':
                    legal.append((current_space, down_right))

            # Space occupied by white
            if col in white_pieces and player == 1:
                # Check down/left & down/right diagonals for empty spaces
                if in_bounds(down_left) and board[row_num+1][col_num-1] == '':
                    legal.append((current_space, down_left))
                if in_bounds(down_right) and board[row_num+1][col_num+1] == '':
                    legal.append((current_space, down_right))

            # Check extra spaces for white kings
            if col == white_king and player == 1:
                # Check up/left & up/right diagonals for empty spaces
                if in_bounds(up_left) and board[row_num-1][col_num-1] == '':
                    legal.append((current_space, up_left))
                if in_bounds(up_right) and board[row_num-1][col_num+1] == '':
                    legal.append((current_space, up_right))

    return legal


def legal_jumps(state):
    board = state['board']
    player = state['player']

    legal = []

    # Iterate over each space on the board
    for row_num, row in enumerate(board):
        for col_num, col in enumerate(row):
            # If the piece belongs to the current player, add its jumps to total list of legal jumps
            if (col in black_pieces and player == 0) or (col in white_pieces and player == 1):
                legal.extend(piece_jumps(state, (row_num, col_num)))

    return legal


def piece_jumps(state, current_space):
    board = state['board']
    row, col = current_space
    piece = board[row][col]

    up_left = (row-2, col-2)
    up_right = (row-2, col+2)
    down_left = (row+2, col-2)
    down_right = (row+2, col+2)

    legal = []

    # Check forward jumps available to black
    if piece in black_pieces:
        # Space two up/left is in bounds and empty
        if in_bounds(up_left) and board[row-2][col-2] == '':
            # Space directly up/left is a white piece
            if board[row-1][col-1] in white_pieces:
                legal.append((current_space, up_left))

        # Space two up/right is in bounds and empty
        if in_bounds(up_right) and board[row-2][col+2] == '':
            # Space directly up/right is a white piece
            if board[row-1][col+1] in white_pieces:
                legal.append((current_space, up_right))

    # Check backward jumps available to black
    if piece == black_king:
        # Space two down/left is in bounds and empty
        if in_bounds(down_left) and board[row+2][col-2] == '':
            # Space directly down/left is a white piece
            if board[row+1][col-1] in white_pieces:
                legal.append((current_space, down_left))
        # Space two down/right is in bounds and empty
        if in_bounds(down_right) and board[row+2][col+2] == '':
            # Space directly down/right is a white piece
            if board[row+1][col+1] in white_pieces:
                legal.append((current_space, down_right))

    # Check forward jumps available to white
    if piece in white_pieces:
        # Space two down/left is in bounds and empty
        if in_bounds(down_left) and board[row + 2][col - 2] == '':
            # Space directly down/left is a black piece
            if board[row + 1][col - 1] in black_pieces:
                legal.append((current_space, down_left))
        # Space two down/right is in bounds and empty
        if in_bounds(down_right) and board[row + 2][col + 2] == '':
            # Space directly down/right is a black piece
            if board[row + 1][col + 1] in black_pieces:
                legal.append((current_space, down_right))

    # Check backward jumps available to white
    if piece == white_king:
        # Space two up/left is in bounds and empty
        if in_bounds(up_left) and board[row - 2][col - 2] == '':
            # Space directly up/left is a black piece
            if board[row - 1][col - 1] in black_pieces:
                legal.append((current_space, up_left))

        # Space two up/right is in bounds and empty
        if in_bounds(up_right) and board[row - 2][col + 2] == '':
            # Space directly up/right is a black piece
            if board[row - 1][col + 1] in black_pieces:
                legal.append((current_space, up_right))

    return legal


def in_bounds(coords):
    row, col = coords
    return True if row in range(0, 8) and col in range(0, 8) else False


def get_next_state(state, move):
    origin, dest = move
    origin_row, origin_col = origin
    dest_row, dest_col = dest
    piece = state['board'][origin_row][origin_col]

    # Determine whether piece should become a king
    if dest_row == 0 or dest_row == 7:
        if piece in black_pieces:
            piece = black_king
        if piece in white_pieces:
            piece = white_king

    # Move piece from origin to destination
    state['board'][origin_row][origin_col] = ''
    state['board'][dest_row][dest_col] = piece

    # If the play was a jump (the piece moved two spaces diagonally
    if abs(origin_row - dest_row) == 2:
        # Remove the jumped piece from the game
        jumped_row = (origin_row + dest_row) // 2
        jumped_col = (origin_col + dest_col) // 2
        state['board'][jumped_row][jumped_col] = ''

        # If no jumps are available to that piece
        if not piece_jumps(state, dest):
            # Change the active player
            state['player'] = 1 if state['player'] == 0 else 0

    # If the play wasn't a jump, it's safe to change the active player
    else:
        state['player'] = 1 if state['player'] == 0 else 0

    return state


def get_winner(state):
    board = state['board']

    num_black = 0
    num_white = 0

    # Iterate through all spaces, count number of each piece
    for row in board:
        for col in row:
            if col in black_pieces:
                num_black += 1
            if col in white_pieces:
                num_white += 1

    # If there are 0 of a color, the other color wins
    if num_black == 0:
        return 1
    elif num_white == 0:
        return 0
    else:
        return -1


def to_string(state):
    board = state['board']

    # Replace empty spots with a space to print properly spaced
    board = [[col if col else ' ' for col in row] for row in board]

    s = '  0 1 2 3 4 5 6 7\n'
    s += ' ┏━┳━┳━┳━┳━┳━┳━┳━┓\n'
    s += '0┃' + '┃'.join(board[0]) + '┃' + '\n'
    s += ' ┣━╋━╋━╋━╋━╋━╋━╋━┫\n'
    s += '1┃' + '┃'.join(board[1]) + '┃' + '\n'
    s += ' ┣━╋━╋━╋━╋━╋━╋━╋━┫\n'
    s += '2┃' + '┃'.join(board[2]) + '┃' + '\n'
    s += ' ┣━╋━╋━╋━╋━╋━╋━╋━┫\n'
    s += '3┃' + '┃'.join(board[3]) + '┃' + '\n'
    s += ' ┣━╋━╋━╋━╋━╋━╋━╋━┫\n'
    s += '4┃' + '┃'.join(board[4]) + '┃' + '\n'
    s += ' ┣━╋━╋━╋━╋━╋━╋━╋━┫\n'
    s += '5┃' + '┃'.join(board[5]) + '┃' + '\n'
    s += ' ┣━╋━╋━╋━╋━╋━╋━╋━┫\n'
    s += '6┃' + '┃'.join(board[6]) + '┃' + '\n'
    s += ' ┣━╋━╋━╋━╋━╋━╋━╋━┫\n'
    s += '7┃' + '┃'.join(board[7]) + '┃' + '\n'
    s += ' ┗━┻━┻━┻━┻━┻━┻━┻━┛\n'

    return s
