from flask import Flask, jsonify, request
from flask_cors import CORS
import rules
import ai

app = Flask(__name__)
CORS(app)


@app.route('/new_game', methods=['GET'])
def new_game():
    return jsonify(rules.new_game)


@app.route('/legal_moves', methods=['POST'])
def legal_moves():
    state = request.json['state']
    return jsonify(rules.get_legal_moves(state))


@app.route('/next_state', methods=['POST'])
def next_state():
    state = request.json['state']
    move = request.json['move']
    return jsonify(rules.get_next_state(state, move))


@app.route('/winner', methods=['POST'])
def winner():
    state = request.json['state']
    return jsonify(rules.get_winner(state))


@app.route('/to_string', methods=['POST'])
def to_string():
    state = request.json['state']
    return jsonify(rules.to_string(state))


@app.route('/best_move', methods=['POST'])
def best_move():
    state = request.json['state']
    return jsonify(ai.montecarlo(state))


if __name__ == '__main__':
    app.run()
