import datetime
import copy
import random
import rules


def montecarlo(state, sim_time=1):
    sim_time = datetime.timedelta(seconds=sim_time)

    legal_moves = rules.get_legal_moves(state)

    wins = {move: 0 for move in legal_moves}

    if not legal_moves:
        return
    if len(legal_moves) == 1:
        return legal_moves[0]

    games = 0

    start_time = datetime.datetime.utcnow()

    while datetime.datetime.utcnow() - start_time < sim_time:
        for move in legal_moves:
            win = montecarlo_simulate(state, move)

            if win:
                wins[move] += 1

            games += 1

    move = max(wins, key=wins.get)

    return move


def montecarlo_simulate(state, move):
    player = state['player']

    state_copy = copy.deepcopy(state)

    state_copy = rules.get_next_state(state_copy, move)

    while rules.get_winner(state_copy) == -1:
        legal_moves = rules.get_legal_moves(state_copy)

        if not legal_moves:
            break

        move = random.choice(legal_moves)

        state_copy = rules.get_next_state(state, move)

    return True if player == rules.get_winner(state_copy) else False
